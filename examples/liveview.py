import sys
from io import BytesIO
from tkinter import Tk, Canvas
from PIL import Image, ImageTk
from time import sleep
from netifaces import AF_INET, ifaddresses

from sonyremoteapi import CameraRemoteAPI, Liveview

WIFI_IFACE = sys.argv[1]

def main():
  root = Tk()
  root.resizable(0,0)
  root.wm_attributes("-topmost", 1)

  canvas = Canvas(root, width=600, height=600, bd=0, highlightthickness=0)
  canvas.pack()

  address = ifaddresses(WIFI_IFACE)[AF_INET][0]['addr']

  camera = CameraRemoteAPI()
  print('Connecting..')
  camera.connect(address)
  print('Connected')
  try:
    camera.startRecMode()
    print('startRecMode is available')
  except:
    pass
  if not camera.is_available('startLiveview'):
    camera.wait_for_methods(['startLiveview'])
  print('startLiveview is available')
  liveview = Liveview(camera)

  try:
    print('Starting Liveview')
    liveview.start()

    while True:
      image_data = liveview.image()
      img = ImageTk.PhotoImage(Image.open(BytesIO(image_data)))
      canvas.create_image(0, 0, image=img)

      root.update_idletasks()
      root.update()

  except Exception as e:
    print(e)
  finally:
    print('Stopping Liveview')
    liveview.stop()
    camera.stopRecMode()
    camera.disconnect()

if __name__ == '__main__':
  main()
