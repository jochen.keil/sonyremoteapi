from enum import Enum, IntEnum

class CameraStatus(Enum):
  Error                        = "Error"
  NotReady                     = "NotReady"
  IDLE                         = "IDLE"
  StillCapturing               = "StillCapturing"
  StillSaving                  = "StillSaving"
  MovieWaitRecStart            = "MovieWaitRecStart"
  MovieRecording               = "MovieRecording"
  MovieWaitRecStop             = "MovieWaitRecStop"
  MovieSaving                  = "MovieSaving"
  AudioWaitRecStart            = "AudioWaitRecStart"
  AudioRecording               = "AudioRecording"
  AudioWaitRecStop             = "AudioWaitRecStop"
  AudioSaving                  = "AudioSaving"
  IntervalWaitRecStart         = "IntervalWaitRecStart"
  IntervalRecording            = "IntervalRecording"
  IntervalWaitRecStop          = "IntervalWaitRecStop"
  LoopWaitRecStart             = "LoopWaitRecStart"
  LoopRecording                = "LoopRecording"
  LoopWaitRecStop              = "LoopWaitRecStop"
  LoopSaving                   = "LoopSaving"
  WhiteBalanceOnePushCapturing = "WhiteBalanceOnePushCapturing"
  ContentsTransfer             = "ContentsTransfer"
  Streaming                    = "Streaming"
  Deleting                     = "Deleting"

class CameraEvent(IntEnum):
  availableApiList      = 0 # "availableApiList"
  cameraStatus          = 1 # "cameraStatus"
  zoomInformation       = 2 # "zoomInformation"
  liveviewStatus        = 3 # "liveviewStatus"
  liveviewOrientation   = 4 # "liveviewOrientation"
  takePicture           = 5 # "takePicture"
  storageInformation    = 10 # "storageInformation"
  beepMode              = 11 # "beepMode"
  cameraFunction        = 12 # "cameraFunction"
  movieQuality          = 13 # "movieQuality"
  stillSize             = 14 # "stillSize"
  cameraFunctionResult  = 15 # "cameraFunctionResult"
  steadyMode            = 16 # "steadyMode"
  viewAngle             = 17 # "viewAngle"
  exposureMode          = 18 # "exposureMode"
  postviewImageSize     = 19 # "postviewImageSize"
  selfTimer             = 20 # "selfTimer"
  shootMode             = 21 # "shootMode"
  exposureCompensation  = 25 # "exposureCompensation"
  flashMode             = 26 # "flashMode"
  fNumber               = 27 # "fNumber"
  focusMode             = 28 # "focusMode"
  isoSpeedRate          = 29 # "isoSpeedRate"
  programShift          = 31 # "programShift"
  shutterSpeed          = 32 # "shutterSpeed"
  whiteBalance          = 33 # "whiteBalance"
  touchAFPosition       = 34 # "touchAFPosition"
  focusStatus           = 35 # "focusStatus"
  zoomSetting           = 36 # "zoomSetting"
  stillQuality          = 37 # "stillQuality"
  contShootingMode      = 38 # "contShootingMode"
  contShootingSpeed     = 39 # "contShootingSpeed"
  contShooting          = 40 # "contShooting"
  flipSetting           = 41 # "flipSetting"
  sceneSelection        = 42 # "sceneSelection"
  intervalTime          = 43 # "intervalTime"
  colorSetting          = 44 # "colorSetting"
  movieFileFormat       = 45 # "movieFileFormat"
  infraredRemoteControl = 52 # "infraredRemoteControl"
  tvColorSystem         = 53 # "tvColorSystem"
  trackingFocusStatus   = 54 # "trackingFocusStatus"
  trackingFocus         = 55 # "trackingFocus"
  batteryInfo           = 56 # "batteryInfo"
  recordingTime         = 57 # "recordingTime"
  numberOfShots         = 58 # "numberOfShots"
  autoPowerOff          = 59 # "autoPowerOff"
  loopRecTime           = 60 # "loopRecTime"
  audioRecording        = 61 # "audioRecording"
  windNoiseReduction    = 62 # "windNoiseReduction"

    # if result[0] is not None:
    #   # self.__update_available_apis(result[0]['names'])
    #   self.__available_api_list = result[0]['names']
    # if result[1] is not None:
    #   status = result[1]['cameraStatus']
    #   if status == "Error":
    #     self.__status.update(SonyCameraStatus.Error)
    #   elif status == "NotReady":
    #     self.__status.update(SonyCameraStatus.NotReady)
    #   elif status == "IDLE":
    #     self.__status.update(SonyCameraStatus.IDLE)
    #   elif status == "StillCapturing":
    #     self.__status.update(SonyCameraStatus.StillCapturing)
    #   elif status == "StillSaving":
    #     self.__status.update(SonyCameraStatus.StillSaving)
    #   elif status == "MovieWaitRecStart":
    #     self.__status.update(SonyCameraStatus.MovieWaitRecStart)
    #   elif status == "MovieRecording":
    #     self.__status.update(SonyCameraStatus.MovieRecording)
    #   elif status == "MovieWaitRecStop":
    #     self.__status.update(SonyCameraStatus.MovieWaitRecStop)
    #   elif status == "MovieSaving":
    #     self.__status.update(SonyCameraStatus.MovieSaving)
    #   elif status == "AudioWaitRecStart":
    #     self.__status.update(SonyCameraStatus.AudioWaitRecStart)
    #   elif status == "AudioRecording":
    #     self.__status.update(SonyCameraStatus.AudioRecording)
    #   elif status == "AudioWaitRecStop":
    #     self.__status.update(SonyCameraStatus.AudioWaitRecStop)
    #   elif status == "AudioSaving":
    #     self.__status.update(SonyCameraStatus.AudioSaving)
    #   elif status == "IntervalWaitRecStart":
    #     self.__status.update(SonyCameraStatus.IntervalWaitRecStart)
    #   elif status == "IntervalRecording":
    #     self.__status.update(SonyCameraStatus.IntervalRecording)
    #   elif status == "IntervalWaitRecStop":
    #     self.__status.update(SonyCameraStatus.IntervalWaitRecStop)
    #   elif status == "LoopWaitRecStart":
    #     self.__status.update(SonyCameraStatus.LoopWaitRecStart)
    #   elif status == "LoopRecording":
    #     self.__status.update(SonyCameraStatus.LoopRecording)
    #   elif status == "LoopWaitRecStop":
    #     self.__status.update(SonyCameraStatus.LoopWaitRecStop)
    #   elif status == "LoopSaving":
    #     self.__status.update(SonyCameraStatus.LoopSaving)
    #   elif status == "WhiteBalanceOnePushCapturing":
    #     self.__status.update(SonyCameraStatus.WhiteBalanceOnePushCapturing)
    #   elif status == "ContentsTransfer":
    #     self.__status.update(SonyCameraStatus.ContentsTransfer)
    #   elif status == "Streaming":
    #     self.__status.update(SonyCameraStatus.Streaming)
    #   elif status == "Deleting":
    #     self.__status.update(SonyCameraStatus.Deleting)
    # if result[5] is not None:
    #   self.__update_postview_urls(result[5]['takePictureUrl'])
