import requests
from threading import Event, Thread
from queue import LifoQueue
from struct import unpack, unpack_from
from sonyremoteapi import CameraRemoteAPI

def common_header(data):
    start_byte, payload_type, sequence_number, time_stamp = unpack('!BBHI', data)
    if start_byte != 255: # 0xff fixed
      raise RuntimeError('wrong livestream start byte')

    common_header = { 'start_byte': start_byte
                    , 'payload_type': payload_type
                    , 'sequence_number': sequence_number
                    , 'time_stamp': time_stamp # milliseconds
                    }

    return common_header

def payload_header(data, payload_type=1):
    # payload_type = 1, assume JPEG
    start_code, jpeg_data_size_2, jpeg_data_size_1, jpeg_data_size_0, padding_size = unpack_from('!IBBBB', data)
    if start_code != 607479929:
      raise RuntimeError('wrong payload header start')

    # This seems silly, but it's a 3-byte-integer !
    jpeg_data_size = jpeg_data_size_0 * 2**0 + jpeg_data_size_1 * 2**8 + jpeg_data_size_2 * 2**16

    payload_header = { 'start_code': start_code
                     , 'jpeg_data_size': jpeg_data_size
                     , 'padding_size': padding_size
                     }

    if payload_type == 1:
      payload_header.update(payload_header_jpeg(data))
    elif payload_type == 2:
      payload_header.update(payload_header_frameinfo(data))
    else:
      raise RuntimeError('Unknown payload type: {}'.format(payload_type))

    return payload_header

def payload_header_jpeg(data):
  reserved_1, flag = unpack_from('!IB', data, offset=8)
  if flag != 0:
    raise RuntimeError('Wrong payload header flag: {}'.format(flag))

  payload_header = { 'reserved_1': reserved_1
                   , 'flag': flag
                   }

  return payload_header

def payload_header_frameinfo(data):
  version, frame_count, frame_size = unpack_from('!HHH', data, offset=8)
  payload_header = { 'version': version
                   , 'frame_count': frame_count
                   , 'frame_size': frame_size
                   }
  return payload_header

def payload_frameinfo(data):
  left, top, right, bottom = unpack_from(">HHHH", data)
  category, status, additional = unpack_from("BBB", data, offset=8)
  payload_frameinfo = { 'left': left
                      , 'top': top
                      , 'right': right
                      , 'bottom': bottom
                      , 'category': category
                      , 'status': status
                      , 'additional': additional
                      }
  return payload_frameinfo

class LiveviewStream(Thread):
  def __init__(self,  url, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.__url = url
    self.__stop = Event()
    self.__frameinfo = []
    self.__head_pool = LifoQueue()
    self.__jpeg_pool = LifoQueue()

  def stop(self):
      self.__stop.set()

  def run(self):
    response = requests.get(self.__url, stream=True)
    stream = response.raw

    while not self.__stop.is_set():
      header = stream.read(8)
      ch = common_header(header)

      data = stream.read(128)
      payload = payload_header(data, payload_type=ch['payload_type'])

      if ch['payload_type'] == 1:
        data_img = stream.read(payload['jpeg_data_size'])
        assert len(data_img) == payload['jpeg_data_size']

        self.__head_pool.put(header)
        self.__jpeg_pool.put(data_img)

      elif ch['payload_type'] == 2:
        self.__frameinfo = []

        for x in range(payload['frame_count']):
          data_img = stream.read(payload['frame_size'])
          self.__frameinfo.append(payload_frameinfo(data_img))

      stream.read(payload['padding_size'])

  def header(self):
    return self.__head_pool.get()

  def image(self):
    return self.__jpeg_pool.get()

  @property
  def frameinfo(self):
    return self.__frameinfo

class Liveview:
  def __init__(self, camera: CameraRemoteAPI):
    self.__camera = camera
    self.__liveview_stream = None

  def start(self):
    '''Start a liveview stream'''
    self.stop()
    url = self.__camera.startLiveview()
    self.__liveview_stream = LiveviewStream(url)
    self.__liveview_stream.start()

  def stop(self) -> None:
    '''Start a liveview stream
    :returns: None
    :rtype: None
    '''
    if self.__liveview_stream is not None:
      self.__liveview_stream.stop()
      self.__liveview_stream.join()
      self.__liveview_stream = None
      self.__camera.stopLiveview()

  def image(self) -> bytes:
    '''Return raw image data from a liveview stream
    :returns: Raw image as bytes
    :rtype: bytes
    '''
    return self.__liveview_stream.image()

  @property
  def frameinfo(self):
    '''Return frameinfo from a liveview stream
    :returns: [frameinfo]
    '''
    return self.__liveview_stream.frameinfo
