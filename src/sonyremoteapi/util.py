from threading import Condition, Lock, Event, Thread

class LockVar:
  def __init__(self, var=None):
    self.__var = var
    self.__lock = Lock()

  @property
  def var(self):
    tmp = None
    with self.__lock:
      tmp = self.__var
    return tmp

  @var.setter
  def var(self, var):
    with self.__lock:
      self.__var = var

class ConditionVar():
  def __init__(self, var):
    self.__var = var
    self.__var_condition = Condition()

  @property
  def var(self):
    with self.__var_condition:
      return self.__var

  @var.setter
  def var(self, var):
    with self.__var_condition:
      self.__var = var
      self.__var_condition.notify_all()

  def wait_for(self, obj, timeout=None) -> bool:
    with self.__var_condition:
      if callable(obj):
        return self.__var_condition.wait_for(lambda: obj(self.__var), timeout)
      else:
        return self.__var_condition.wait_for(lambda: self.__var == obj, timeout)

class Producer(Thread):
  def __init__(self,  producer, consumer, *args, **kwargs):
      super().__init__(*args, **kwargs)
      self.__producer = producer
      self.__consumer = consumer
      self.__stop = Event()

  def stop(self):
      self.__stop.set()

  def run(self):
    while not self.__stop.is_set():
      try:
        self.__consumer(self.__producer())
      except:
        pass
