import requests
from ssdpy import SSDPClient
from xml.etree import ElementTree
from .util import LockVar, ConditionVar, Producer
from .parameter import CameraEvent, CameraStatus

def get_device(address=None):
  client = SSDPClient(address=address)
  devices = client.m_search(st=CameraRemoteAPI.URN)

  for device in devices:
    if device['st'] == CameraRemoteAPI.URN:
      return device

  raise RuntimeError("Could not find device")

def get_endpoints(device):
  try:
    response = requests.get(device['location'])
    tree = ElementTree.fromstring(response.content)
  except Exception as e:
    raise RuntimeError("Could not get device location", e)

  try:
    endpoints = dict()
    namespace = {'av': 'urn:schemas-sony-com:av'}
    for e in tree.findall('.//av:X_ScalarWebAPI_Service', namespace):
      serviceType = e.find('av:X_ScalarWebAPI_ServiceType', namespace).text
      serviceLocation = e.find('av:X_ScalarWebAPI_ActionList_URL', namespace).text
      endpoints[serviceType] = serviceLocation
  except Exception as e:
    raise RuntimeError("No endpoints available", e)

  return endpoints

def get_url(endpoints, service_type):
  # service_type, action_list_url = endpoint
  return '{}/{}'.format(endpoints[service_type], service_type)

def get_request(method, version, *args, id_=1):
  return { 'method': method
         , 'params': [*args]
         , 'id': id_
         , 'version': version
         }

def get_result(response):
  '''Returns the reponse for an API call
  :raises SonyApiException:
  :raises HTTPError:
  :raises JSONDecodeError:
  '''
  def result(r):
    if len(r) == 0:
      return None
    elif len(r) == 1:
      return r[0]
    else:
      return r

  if response.status_code is not requests.codes.ok:
    response.raise_for_status()
  data = response.json()
  if 'result' in data:
    return result(data['result'])
  elif 'results' in data:
    return result(data['results'])
  elif 'error' in data:
    error_id = response.json()['error'][0]
    error_txt = response.json()['error'][1]
    raise SonyApiException(error_id, error_txt)
  else:
    raise RuntimeError(f'Could not get result from {response}', response)

def call(endpoints, service_type, method, version, *args, timeout=None):
  url = get_url(endpoints, service_type)
  request = get_request(method, version, *args)
  response = requests.post(url, json=request, timeout=timeout)
  return get_result(response)

class SonyApiException(Exception):
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

class CameraRemoteAPI():
  URN = 'urn:schemas-sony-com:service:ScalarWebAPI:1'

  def __init__(self, timeout=None):
    '''
    https://docs.python-requests.org/en/master/user/advanced/#timeouts
    recommended timeout = 3.05
    '''
    self.__timeout = timeout
    self.__endpoints = dict()
    self.__method_types = dict()
    self.__event_handler = dict()
    self.__status_handler = dict()
    self.__event_producer = None
    self.__available_api_list = ConditionVar(list())
    self.__camera_status = ConditionVar(CameraStatus.NotReady)

    for e in CameraEvent:
      self.__event_handler[e] = list()

    for s in CameraStatus:
      self.__status_handler[s] = list()

    self.on_event(CameraEvent.cameraStatus, self.__camera_status_handler)
    self.on_event(CameraEvent.availableApiList, self.__available_api_list_handler)

  # https://stackoverflow.com/a/534597
  def __add_method(self, method):
    service_type, version = self.__method_types[method]
    def method_call(*args, version=version, timeout=None):
      return call(self.__endpoints, service_type, method, version, *args, timeout=timeout)
    setattr(self, method, method_call)

  def __update_methods(self):
    for method_name in self.__method_types.keys():
      try:
        delattr(self, method_name)
      except:
        pass
    for method in self.__available_api_list.var:
      self.__add_method(method)

  def __event_listener(self):
    return self.getEvent(True, timeout=self.__timeout)

  def __event_dispatcher(self, result):
    for e, data in enumerate(result):
      if data is not None:
        for handler in self.__event_handler[e]:
          handler(data)

  def __camera_status_handler(self, data):
    status = CameraStatus(data['cameraStatus'])
    self.__camera_status.var = status
    for handler in self.__status_handler[status]:
      handler(status)

  def __available_api_list_handler(self, data):
    self.__available_api_list.var = data['names']
    self.__update_methods()

  def on_event(self, e: CameraEvent, handler):
    self.__event_handler[e].append(handler)

  def on_status(self, s: CameraStatus, handler):
    self.__status_handler[s].append(handler)

  def service(self, method):
    service, _ = self.__supported_api[method]
    return service

  def available_api_list(self):
    return self.__available_api_list.var

  def is_available(self, method):
    return method in self.__available_api_list.var

  def wait_for_methods(self, methods, timeout=None):
    available_apis = self.__available_api_list.var
    if not set(methods).issubset(available_apis):
      return self.__available_api_list.wait_for(lambda apis: set(methods).issubset(apis))
    else:
      return True

  def status(self):
    return self.__camera_status.var

  def wait_for_status(self, status, timeout=None) -> bool:
    '''Wait until the camera is in a certain status. Blocking call.
    :param status: Which camera status to wait for (`SonyCameraStatus`)
    :type status: SonyCameraStatus
    :returns: True when the requested status has been set.
    :rtype: bool
    '''
    if self.__camera_status.var != status:
      return self.__camera_status.wait_for(status, timeout)
    else:
      return True

  def call(self, method, *args, service_type=None, version=None, timeout=None):
    '''Call the API `method` at `service` with (optional) `params`
    :returns: The `result` field from the response
    :raises RuntimeError:
    :raises SonyApiException:
    :raises HTTPError:
    :raises JSONDecodeError:
    '''
    if service_type is None:
      service_type, _ = self.__method_types[method]
    if version is None:
      _, version = self.__method_types[method]
    return call(self.__endpoints, service_type, method, version, *args, timeout=timeout)

  def connect(self, address=None):
    devices = get_device(address=address)
    self.__endpoints = get_endpoints(devices)

    for service_type in self.__endpoints.keys():
      method_types = call(self.__endpoints, service_type, 'getMethodTypes', '1.0', '')
      for method_type in method_types:
        method_name = method_type[0]
        method_version = method_type[3]
        v = (service_type, method_version)

        if method_name in self.__method_types:
          if float(method_version) > float(self.__method_types[method_name][1]):
            v = (service_type, method_version)

        self.__method_types[method_name] = v

    self.__available_api_list.var = self.call('getAvailableApiList')
    self.__update_methods()

    if self.is_available('getEvent'):
      self.__event_producer = Producer(self.__event_listener, self.__event_dispatcher, daemon=True)
      self.__event_producer.start()

  def disconnect(self):
    if self.__event_producer and self.__event_producer.is_alive():
      self.__event_producer.stop()
      self.__event_producer.join()

    self.__event_producer = None

    self.__camera_status = ConditionVar(CameraStatus.NotReady)
    self.__available_api_list = ConditionVar(list())
    self.__update_methods()

def main():
  from time import sleep
  from datetime import datetime

  def log(*msg):
    t = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
    print(t, *msg)

  def print_status(data):
    print(data['cameraStatus'])

  def idle_handler(status):
    print('idle handler', status)

  def not_ready_handler(status):
    print('not ready handler', status)

  log('Connecting')
  camera = CameraRemoteAPI(timeout=3.05)

  camera.on_event(CameraEvent.cameraStatus, print_status)
  camera.on_status(CameraStatus.IDLE, idle_handler)
  camera.on_status(CameraStatus.NotReady, not_ready_handler)

  camera.connect('192.168.122.22')
  log('Connecting finished')


  methods = [ 'getAvailableFNumber'
            , 'getAvailableShutterSpeed'
            , 'getAvailableIsoSpeedRate'
            , 'setFNumber'
            , 'setShutterSpeed'
            , 'setIsoSpeedRate'
            ]

  try:
    log('startRecMode start')
    camera.startRecMode()
    log('startRecMode finished')

    log('waiting for methods')
    if camera.wait_for_methods(methods):
      log('methods available')
    else:
      raise RuntimeError('methods not available')

    available_fnumber = camera.getAvailableFNumber()
    available_shutter_speed = camera.getAvailableShutterSpeed()
    available_iso_speed_rate = camera.getAvailableIsoSpeedRate()

    print(available_fnumber)
    print(available_shutter_speed)
    print(available_iso_speed_rate)

    for fnumber in available_fnumber[1]:
      print('setting fnumber', fnumber)
      camera.setFNumber(fnumber)
      sleep(0.5)

    for shutter_speed in filter(lambda s: s != 'BULB', available_shutter_speed[1]):
      print('setting shutter speed', shutter_speed)
      camera.setShutterSpeed(shutter_speed)
      sleep(0.1)

    for iso_speed_rate in filter(lambda i: i != 'AUTO', available_iso_speed_rate[1]):
      print('setting iso speed rate', iso_speed_rate)
      camera.setIsoSpeedRate(iso_speed_rate)
      sleep(0.1)

    sleep(2)

  finally:
    camera.stopRecMode()

  camera.disconnect()

if __name__ == '__main__':
  main()
